# steal

#### 介绍
爬取抖音直播间弹幕

#### 软件架构
springboot 2.7.0
selenium 4.0.0


#### 安装教程

1.  git clone 项目
2.  将本地电脑或者服务器的谷歌版本驱动一一对应，也就是driver文件的驱动要对应你的谷歌浏览器
3.  将抖音直播间填充进去，获取弹幕


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
