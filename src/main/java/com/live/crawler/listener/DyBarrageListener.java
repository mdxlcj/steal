package com.live.crawler.listener;

import com.live.crawler.entity.DyBarrageMessage;

/**
 * 抖音监听
 * @author chengjian 
 * @date 2022/6/18
 */
public interface DyBarrageListener {
    /**
     * 消费
     * @author chengjian
     * @date 2022/6/18 6:19 下午
     * @param barrageMessage 抖音弹幕
     */
    void consume(DyBarrageMessage barrageMessage);
}
