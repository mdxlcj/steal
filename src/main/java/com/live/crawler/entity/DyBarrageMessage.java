package com.live.crawler.entity;

import lombok.Data;
import lombok.ToString;
/**
 * 抖音弹幕信息
 * @author chengjian
 * @date 2022/6/18 6:11 下午
 */
@Data
@ToString(of = {"nickname","message"})
public class DyBarrageMessage {
    /**
     * 直播间弹幕
     */
    private String message;
    /**
     * 抖音昵称
     */
    private String nickname;
    /**
     * 抖音用户id
     */
    private String id;
    private String avatar;
}
